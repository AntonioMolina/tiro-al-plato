#include "ControlsState.h"

template<> ControlsState* Singleton<ControlsState>::msSingleton = 0;

ControlsState::ControlsState () : _sheet(nullptr), _root(nullptr),
        _sceneMgr(nullptr), _camera(nullptr), _back(false) {
}

void ControlsState::enter () {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _back = false;
    createGUI();
}

void ControlsState::exit () {
    _root->getAutoCreatedWindow()->removeAllViewports();
    deleteGUI();
}

void ControlsState::pause () {}

void ControlsState::resume () {}

void ControlsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
        case OIS::KC_H:
            _back = true;
            break;
        default:
            break;
    }
}

void ControlsState::keyReleased (const OIS::KeyEvent &e) {}

void ControlsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void ControlsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonDown(convertMouseButton(id));
}

void ControlsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonUp(convertMouseButton(id));
}

bool ControlsState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool ControlsState::frameEnded (const FrameEvent& evt) {
    if (_back) {
        popState();
    }

    return true;
}

ControlsState& ControlsState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

ControlsState* ControlsState::getSingletonPtr () {
    return msSingleton;
}

void ControlsState::createGUI () {
    _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Controls");

    /* Back Menu button */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");
    backButton->setText("Back");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ControlsState::back, this));

    

    //CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);

CEGUI::Window* controles = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "Controles");
    controles->setText("[colour='FFFFFFFF'][font='DejaVuSans-25']How to Play: \nMouse displacement for move the camera \nClick mouse for shoot");
    controles->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.2,0)));
    controles->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.7/2,0),CEGUI::UDim(0.35,0)));
	_sheet->addChild(backButton);
    _sheet->addChild(controles);

 	CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);

}

void ControlsState::deleteGUI () {
    _sheet->destroyChild("BackButton");

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->destroyChild(_sheet);
}

bool ControlsState::back (const CEGUI::EventArgs &e) {
    _back = true;
    return true;
}

CEGUI::MouseButton ControlsState::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}
