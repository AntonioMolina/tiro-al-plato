#include "RecordsState.h"

#include "RecordsManager.h"

template<> RecordsState* Singleton<RecordsState>::msSingleton = 0;

RecordsState::RecordsState () : _sheet(nullptr), _root(nullptr),
        _sceneMgr(nullptr), _camera(nullptr), _back(false) {
}

void RecordsState::enter () {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _back = false;
    createGUI();
}

void RecordsState::exit () {
    _root->getAutoCreatedWindow()->removeAllViewports();
    deleteGUI();
}

void RecordsState::pause () {}

void RecordsState::resume () {}

void RecordsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
        case OIS::KC_R:
            _back = true;
            break;
        default:
            break;
    }
}

void RecordsState::keyReleased (const OIS::KeyEvent &e) {}

void RecordsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void RecordsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonDown(convertMouseButton(id));
}

void RecordsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonUp(convertMouseButton(id));
}

bool RecordsState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool RecordsState::frameEnded (const FrameEvent& evt) {
    if (_back) {
        popState();
    }

    return true;
}

RecordsState& RecordsState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

RecordsState* RecordsState::getSingletonPtr () {
    return msSingleton;
}

void RecordsState::createGUI () {
    _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Credits");

    /* Back Menu button */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");
    backButton->setText("Back");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RecordsState::back, this));

    stringstream rcdsString;
    rcdsString.str("");

    rcdsString << "[colour='FFD7D7D7'][font='DejaVuSans-25']Position      Points\n";

    string s = RecordsManager::getSingletonPtr()->createStringRecords();
    std::vector<string> rcds = RecordsManager::getSingletonPtr()->split(s, '\n');

    for (uint i = 1; i <= rcds.size(); i++) {
        string aux = rcds[i - 1];
        std::vector<string> v = RecordsManager::getSingletonPtr()->split(aux, ' ');
        rcdsString << v[0] << "              " << v[1] << "\n";
    }

    CEGUI::Window* recordsLabel = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "RecordsLabel");
    recordsLabel->setText(rcdsString.str());
    recordsLabel->setSize(CEGUI::USize(CEGUI::UDim(0.45,0),CEGUI::UDim(0.45,0)));
    recordsLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.45/2,0),CEGUI::UDim(0.15,0)));

    _sheet->addChild(backButton);
    _sheet->addChild(recordsLabel);

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);
}

void RecordsState::deleteGUI () {
    _sheet->destroyChild("BackButton");
    _sheet->destroyChild("RecordsLabel");

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->destroyChild(_sheet);
}

bool RecordsState::back (const CEGUI::EventArgs &e) {
    _back = true;
    return true;
}

CEGUI::MouseButton RecordsState::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}
