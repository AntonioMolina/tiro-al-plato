#include "RecordsManager.h"

template<> RecordsManager* Singleton<RecordsManager>::msSingleton = 0;

RecordsManager::RecordsManager () {
    createRecordsFile();
}

bool RecordsManager::saveRecords (int puntuation) {
    string line;
    fstream recordsFile;

    std::vector<int> records;
    records.reserve(5);

    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);

    if (recordsFile.good()) {
        while (getline(recordsFile, line)) {
            if (line.size() > 0) {
                try {
                    int puntos = atoi(line.c_str());
                    records.push_back(puntos);
                } catch (...) {
                    cout << "No se ha podido leer el dato. Error" << endl;
                }
            }
        }
    }

    recordsFile.close();

    bool top5 = false;
    if (puntuation > records.at(records.size() - 1)) {
        top5 = true;
    }

    records.push_back(puntuation);

    sort(records.begin(), records.end(), std::greater<int>());

    recordsFile.open("records.rcd", ios::out | ios::trunc | ios::binary);

    for (unsigned int i = 0; i < records.size(); i++) {
        if (i < 5) {
            recordsFile << records.at(i) << endl;
        } else {
            break;
        }
    }

    recordsFile.close();

    return top5;
}

void RecordsManager::createRecordsFile () {
    fstream recordsFile;
    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);
    if (!recordsFile.good()) {
        recordsFile.close();
        recordsFile.open("records.rcd", ios::out | ios::trunc | ios::binary);

        recordsFile.close();
    } else {
        recordsFile.close();
    }
}

string RecordsManager::createStringRecords () {
    stringstream res;
    res.str("");
    fstream recordsFile;
    string line;

    std::vector<int> records;
    records.reserve(5);

    recordsFile.open("records.rcd", ios::in | ios::out | ios::binary);

    int i;
    if (recordsFile.good()) {
        /* Se guardan primero en un vector */
        i = 0;
        while (getline(recordsFile, line)) {
            /* Solo se leen los 5 primeros datos. */
            if (i < 5) {
                if (line.size() > 0) {
                    try {
                        int puntos = atoi(line.c_str());
                        records.push_back(puntos);
                        i++;
                    } catch (...) {
                        cout << "No se ha podido leer el dato. Error" << endl;
                    }
                } else {
                    records.push_back(0);
                    i++;
                }
            }
        }

        while (i < 5) {
            records.push_back(0);
            i++;
        }

        i = 0;
        while (i < 5) {
            res << (i + 1) << " " << records.at(i) << "\n";
            i++;
        }
    }

    recordsFile.close();

    return res.str();
}

RecordsManager& RecordsManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

RecordsManager* RecordsManager::getSingletonPtr () {
    return msSingleton;
}

std::vector<string> RecordsManager::split (std::string str, char delimiter) {
    std::vector<string> internal;
    stringstream ss(str);
    string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
}
