#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <Ogre.h>

#include "GameManager.h"

#include "ControlsState.h"
#include "CreditsState.h"
#include "GameOverState.h"
#include "IntroState.h"
#include "MenuState.h"
#include "PauseState.h"
#include "PlayState.h"
#include "RecordsState.h"
#include "RecordsManager.h"

using namespace Ogre;
using namespace std;

int main () {
    srand (time(0));

    GameManager* game = new GameManager();

    new ControlsState;
    new CreditsState;
    new GameOverState;
    new IntroState;
    new MenuState;
    new PauseState;
    new PlayState;
    new RecordsState;
    new RecordsManager;

    try {
        game->start(IntroState::getSingletonPtr());
    } catch ( Exception& e ) {
        cerr << "Excepción detectada: " << e.getFullDescription();
    }

    return 0;
}
