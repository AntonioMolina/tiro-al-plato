#include "PlayState.h"

#include "GameManager.h"

#include "GameOverState.h"
#include "PauseState.h"
#include "RecordsManager.h"

#include "Constants.h"

template<> PlayState* Singleton<PlayState>::msSingleton = 0;

PlayState::PlayState () {
}

void PlayState::enter () {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");

    _camera = _sceneMgr->getCamera("MainCamera");
    _camera->lookAt(Vector3(0, 4.75, 0));

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _pause = false;

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
    createGUI();

	createOverlay();

    _game = new Game();
    _sceneMgr->setSkyBox(true, "SkyBox", 500, false);
    //_sceneMgr->setFog(Ogre::FOG_LINEAR, ColourValue(0.9, 0.9, 0.9), 0.0, 10, 500);

    /* Hide mouse cursor. */
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor()
        .setPosition(CEGUI::Vector2f(_root->getAutoCreatedWindow()->getWidth()/2,
        _root->getAutoCreatedWindow()->getHeight()/2));
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();


    GameManager::getSingletonPtr()->getTrackMenu()->stop();
    GameManager::getSingletonPtr()->getTrackGame()->play();
}

void PlayState::exit () {
    deleteGUI();
    destroyOverlay();
    deleteGame();

    //_sceneMgr->setSkyBoxEnabled(false);
    _camera->lookAt(Ogre::Vector3(0, 4.75, 0));

    _root->getAutoCreatedWindow()->removeAllViewports();
    /* Show mouse cursor.*/
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();

    GameManager::getSingletonPtr()->getTrackGame()->stop();
    GameManager::getSingletonPtr()->getTrackMenu()->play();
}

void PlayState::pause () {
    destroyOverlay();
    /* Show mouse cursor. */
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
}

void PlayState::resume () {
    createOverlay ();
    _pause = false;
    /* Hide mouse cursor. */
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
}

void PlayState::keyPressed (const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_ESCAPE || e.key == OIS::KC_P) {
        _pause = true;
    }
}

void PlayState::keyReleased (const OIS::KeyEvent &e) {
}

void PlayState::mouseMoved (const OIS::MouseEvent &e) {
    Real x = e.state.X.rel;
    Real y = e.state.Y.rel;

    Real rot_x = x * _deltaT * -1;
    Real rot_y = y * _deltaT * -1;

    _camera->yaw(Radian(rot_x));
    _camera->pitch(Radian(rot_y));
}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    if (id == OIS::MB_Left) {
        _game->Shoot();
    }
}

void PlayState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
}

bool PlayState::frameStarted (const FrameEvent& evt) {
    _deltaT = evt.timeSinceLastFrame;

    _game->UpdateWorld(_deltaT);

    return true;
}

bool PlayState::frameEnded (const FrameEvent& evt) {
    _deltaT = evt.timeSinceLastFrame;

    updateGUI();
    repositionCamera();

    if (_game->getEndGame()) {
        deleteGUI();
        bool top5 = RecordsManager::getSingletonPtr()->saveRecords(_game->getPuntuation());
        GameOverState::getSingletonPtr()->setData(_game->getPuntuation(), top5);
        pushState(GameOverState::getSingletonPtr());
    }

    if (_pause) {
        pushState(PauseState::getSingletonPtr());
    }

    return true;
}

PlayState& PlayState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

PlayState* PlayState::getSingletonPtr () {
    return msSingleton;
}

void PlayState::repositionCamera () {
    Vector3 dir = _camera->getDirection();

    if (dir.x < -0.5) {
        dir.x = -0.5;
    }
    if (dir.x > 0.5) {
        dir.x = 0.5;
    }

    if (dir.y < -0.5) {
        dir.y = -0.5;
    }
    if (dir.y > 0.4) {
        dir.y = 0.4;
    }

    _camera->setDirection(dir);
}

void PlayState::createGUI () {
    CEGUI::Window* puntuation = CEGUI::WindowManager::getSingleton().
        createWindow("TaharezLook/Label", "PuntuationLabel");
    puntuation->setSize(CEGUI::USize(CEGUI::UDim(0.2,0), CEGUI::UDim(0.1,0)));
    puntuation->setPosition(CEGUI::UVector2(CEGUI::UDim(0.7,0), CEGUI::UDim(0.05,0)));
    _sheet->addChild(puntuation);

    CEGUI::Window* timeleft = CEGUI::WindowManager::getSingleton().
        createWindow("TaharezLook/Label", "TimeLeftLabel");
    timeleft->setSize(CEGUI::USize(CEGUI::UDim(0.4,0), CEGUI::UDim(0.3,0)));
    timeleft->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5 - 0.4/2,0), CEGUI::UDim(0.05,0)));
    _sheet->addChild(timeleft);

    CEGUI::Window* attempts = CEGUI::WindowManager::getSingleton().
        createWindow("TaharezLook/Label", "AttemptsLabel");
    attempts->setSize(CEGUI::USize(CEGUI::UDim(0.2,0), CEGUI::UDim(0.1,0)));
    attempts->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05,0), CEGUI::UDim(0.05,0)));
    _sheet->addChild(attempts);
}

void PlayState::deleteGUI () {
    try {
        _sheet->destroyChild("PuntuationLabel");
        _sheet->destroyChild("TimeLeftLabel");
        _sheet->destroyChild("AttemptsLabel");
    } catch(...) {
    }

}

void PlayState::updateGUI () {
    CEGUI::Window* puntuation = _sheet->getChild("PuntuationLabel");
    std::stringstream s_puntuation;
    s_puntuation << "[colour='FF000000'][colour='FF000000']Round: " << _game->getRound() <<
        "\nPuntuation: " << _game->getPuntuation();
    puntuation->setText(s_puntuation.str());

    CEGUI::Window* timeleft = _sheet->getChild("TimeLeftLabel");
    std::stringstream s_time;

    Real timeGUI = 0;
    switch (_game->getState()) {
        case WAITING_NEXT_ROUND:
            timeGUI = _game->getTimeLeftNextRound();
            s_time << "[colour='FF000000'][colour='FF000000']Time for next Round: ";
            break;
        case PLAYING:
            timeGUI = _game->getTimeLeftNextPlate();
            s_time << "[colour='FF000000']Time for next plate: ";
            break;
    }
    if (timeGUI < 1) {
        s_time.precision(1);
    } else {
        s_time.precision(2);
    }

    s_time << timeGUI;
    timeleft->setText(s_time.str());

    CEGUI::Window* attempts = _sheet->getChild("AttemptsLabel");
    std::stringstream s_attemps;
    s_attemps << "[colour='FF000000']Remaining attempts: " << (maxAttempts - _game->getAttempts());
    attempts->setText("" + s_attemps.str());
}

CEGUI::MouseButton PlayState::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}

void PlayState::createOverlay() {
  _overlayManager = OverlayManager::getSingletonPtr();
  Overlay *overlay = _overlayManager->getByName("Info");
  overlay->show();
}

void PlayState::destroyOverlay () {
    Overlay *overlay = _overlayManager->getByName("Info");
    overlay->hide();
}
