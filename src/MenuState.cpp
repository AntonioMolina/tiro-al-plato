#include "MenuState.h"

#include "ControlsState.h"
#include "CreditsState.h"
#include "PlayState.h"
#include "RecordsState.h"

template<> MenuState* Singleton<MenuState>::msSingleton = 0;

MenuState::MenuState () : _sheet(nullptr), _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _exit(false) {
}

void MenuState::enter () {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _exit = false;

    createBackground();
    createGUI();
}

void MenuState::exit () {
    _root->getAutoCreatedWindow()->removeAllViewports();
    deleteGUI();
    deleteBackground();
}

void MenuState::pause () {
    _root->getAutoCreatedWindow()->removeAllViewports();
    deleteGUI();
}

void MenuState::resume () {
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _exit = false;
    createGUI();
}

void MenuState::keyPressed (const OIS::KeyEvent &e) {
    /* Transition to the next state.
       Space --> PlayState
       C -> CreditsState
       R -> RecordsState
       H -> ControlsState
       ESCAPE -> Exit game */
    switch (e.key) {
        case OIS::KC_SPACE:
            changeState(PlayState::getSingletonPtr());
            break;
        case OIS::KC_C:
            pushState(CreditsState::getSingletonPtr());
            break;
        case OIS::KC_R:
            pushState(RecordsState::getSingletonPtr());
            break;
        case OIS::KC_H:
            pushState(ControlsState::getSingletonPtr());
            break;
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        default:
            break;
    }
}

void MenuState::keyReleased (const OIS::KeyEvent &e) {
}

void MenuState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void MenuState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonDown(convertMouseButton(id));
}

void MenuState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonUp(convertMouseButton(id));
}

bool MenuState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool MenuState::frameEnded (const FrameEvent& evt) {
    return !_exit;
}

MenuState& MenuState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

MenuState* MenuState::getSingletonPtr () {
    return msSingleton;
}

void MenuState::createBackground () {
    MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
    material->getTechnique(0)->getPass(0)->createTextureUnitState("BackGroundMenu.jpg");
    material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
    material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
    material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

    Rectangle2D* rect = new Ogre::Rectangle2D(true);
    rect->setCorners(-1.0, 1.0, 1.0, -1.0);
    rect->setMaterial("Background");

    rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);

    AxisAlignedBox aabInf;
    aabInf.setInfinite();
    rect->setBoundingBox(aabInf);

    SceneNode* node = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundMenu");
    node->attachObject(rect);
}

void MenuState::deleteBackground () {
    _sceneMgr->clearScene();//destroySceneNode("BackgroundMenu");
}

void MenuState::createGUI () {
    _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Menu");

    /* Play Button */
    CEGUI::Window* playButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "PlayButton");
    playButton->setText("Play");
    playButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    playButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.4,0)));
    playButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::start, this));

    /* Controls button */
    CEGUI::Window* controlsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "ControlsButton");
    controlsButton->setText("How to Play");
    controlsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    controlsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.5,0)));
    controlsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::controls, this));

    /* Records button */
    CEGUI::Window* recordsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "RecordsButton");
    recordsButton->setText("Records");
    recordsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    recordsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.6,0)));
    recordsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::records, this));

    /* Creditos button */
    CEGUI::Window* creditsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "CreditsButton");
    creditsButton->setText("Credits");
    creditsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::credits, this));

    /* Exit button */
    CEGUI::Window* exitButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "ExitButton");
    exitButton->setText("Exit");
    exitButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    exitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.8,0)));
    exitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::quit, this));

    _sheet->addChild(playButton);
    _sheet->addChild(controlsButton);
    _sheet->addChild(recordsButton);
    _sheet->addChild(creditsButton);
    _sheet->addChild(exitButton);

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);
}

void MenuState::deleteGUI () {
    _sheet->destroyChild("PlayButton");
    _sheet->destroyChild("ControlsButton");
    _sheet->destroyChild("RecordsButton");
    _sheet->destroyChild("CreditsButton");
    _sheet->destroyChild("ExitButton");

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->destroyChild(_sheet);
}

bool MenuState::start (const CEGUI::EventArgs &e) {
    changeState(PlayState::getSingletonPtr());
    return true;
}

bool MenuState::quit (const CEGUI::EventArgs &e) {
    _exit = true;
    return true;
}

bool MenuState::credits (const CEGUI::EventArgs &e) {
    pushState(CreditsState::getSingletonPtr());
    return true;
}

bool MenuState::records (const CEGUI::EventArgs &e) {
    pushState(RecordsState::getSingletonPtr());
    return true;
}

bool MenuState::controls (const CEGUI::EventArgs &e) {
    pushState(ControlsState::getSingletonPtr());
    return true;
}

CEGUI::MouseButton MenuState::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}
