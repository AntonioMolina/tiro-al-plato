#include "Plate.h"

Plate::Plate (unsigned int id, Entity* ent, SceneNode* node, CollisionShape* shape,
        RigidBody* body) : _id(id), _entPlate(ent), _nodePlate(node), _shapePlate(shape),
        _bodyPlate(body) {
}

Plate::~Plate () {
    if (_shapePlate) {
        delete _shapePlate;
    }

    if (_bodyPlate) {
        delete _bodyPlate;
    }

    if (_entPlate) {
        delete _entPlate;
    }

    if (_nodePlate) {
        delete _nodePlate;
    }
}
