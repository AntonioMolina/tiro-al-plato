#include "Cartridge.h"

Cartridge::Cartridge (unsigned int id, Entity* ent, SceneNode* node, CollisionShape* shape,
    RigidBody* body) : _id(id), _entPlate(ent), _nodePlate(node), _shapePlate(shape),
    _bodyPlate(body) {

}

Cartridge::~Cartridge () {
    delete _shapePlate;
    delete _bodyPlate;
    delete _entPlate;
    delete _nodePlate;
}
