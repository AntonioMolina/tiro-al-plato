#include "IntroState.h"

#include "GameManager.h"
#include "MenuState.h"

template<> IntroState* Singleton<IntroState>::msSingleton = 0;

IntroState::IntroState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr) {
}

void IntroState::enter () {
    _root = Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");
    _sceneMgr->setAmbientLight(ColourValue(1, 1, 1));

    SceneNode* nodeCamera = _sceneMgr->getRootSceneNode()->createChildSceneNode("CameraNode");

    nodeCamera->setPosition(Vector3(0, 4.75, 1));

    _camera = _sceneMgr->createCamera("MainCamera");
    _camera->setPosition(Vector3(0, 4.75, 1));
    _camera->lookAt(Vector3(0, 4.75, 0));
    _camera->setNearClipDistance(0.1);
    _camera->setFarClipDistance(1000);

    nodeCamera->attachObject(_camera);

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    setCEGUI();

    GameManager::getSingletonPtr()->getTrackMenu()->play();
}

void IntroState::exit () {
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause () {
}

void IntroState::resume () {
}

void IntroState::keyPressed (const OIS::KeyEvent &e) {}

void IntroState::keyReleased (const OIS::KeyEvent &e) {}

void IntroState::mouseMoved (const OIS::MouseEvent &e) {}

void IntroState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void IntroState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

bool IntroState::frameStarted (const FrameEvent& evt) {
    changeState(MenuState::getSingletonPtr());
    return true;
}

bool IntroState::frameEnded (const FrameEvent& evt) {
    return false;
}

IntroState& IntroState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

IntroState* IntroState::getSingletonPtr () {
    return msSingleton;
}

void IntroState::setCEGUI () {
    CEGUI::OgreRenderer::bootstrapSystem();
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
    CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

    CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");

    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Tiro Al Plato");
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);
}
