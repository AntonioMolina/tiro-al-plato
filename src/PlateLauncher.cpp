#include "PlateLauncher.h"

PlateLauncher::PlateLauncher (const string name, Vector3 position) {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");

    _entPlateLauncher = _sceneMgr->createEntity("entity_" + name, "cube.mesh");
    _entPlateLauncher->setMaterialName("cube");

    _nodePlateLauncher = _sceneMgr->createSceneNode(name);
    _nodePlateLauncher->attachObject(_entPlateLauncher);

    _cameraPlateLauncher = _sceneMgr->createCamera("camera_" + name);
    _cameraPlateLauncher->setPosition(position);

    if (position.x < 0) {
        _cameraPlateLauncher->lookAt(position + Vector3(0, 0, -1));
    } else {
        _cameraPlateLauncher->lookAt(position + Vector3(0, 0, 1));
    }

    _minRotY = 17;
    _maxRotY = 62;

    _minRotX = 25;
    _maxRotX = 30;

    _nodePlateLauncher->attachObject(_cameraPlateLauncher);

    _nodePlateLauncher->setPosition(position);
    _nodePlateLauncher->scale(Vector3(0.5, 0.5, 0.5));

    _sceneMgr->getSceneNode("GameNode")->addChild(_nodePlateLauncher);
}

PlateLauncher::~PlateLauncher () {
}

void PlateLauncher::rotate ()  {
    Degree rotation_pitch;

    if (_nodePlateLauncher->getPosition().x < 0) {
        _nodePlateLauncher->yaw(Degree(-(rand() % (_maxRotY - _minRotY) - _minRotY)));
        rotation_pitch = Degree(rand() % (_maxRotX - _minRotX) + _minRotX);
        _nodePlateLauncher->pitch(rotation_pitch);
    } else {
        _nodePlateLauncher->yaw(Degree(rand() % (_maxRotY - _minRotY) - _minRotY));
        rotation_pitch = Degree(-(rand() % (_maxRotX - _minRotX) + _minRotX));
        _nodePlateLauncher->pitch(rotation_pitch);
    }
}
