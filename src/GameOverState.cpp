#include "GameOverState.h"

#include "PlayState.h"
#include "MenuState.h"

template<> GameOverState* Singleton<GameOverState>::msSingleton = 0;

void GameOverState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

    createGUI();

    _puntuation = 0;
    _top5 = false;

    _playAgain = false;
    _goMenu = false;
}

void GameOverState::exit () {
    deleteGUI();
}

void GameOverState::pause () {
}

void GameOverState::resume () {
}

void GameOverState::keyPressed (const OIS::KeyEvent &e) {
    if (e.key == OIS::KC_SPACE) {
        _playAgain = true;
    }
    if (e.key == OIS::KC_ESCAPE) {
        _goMenu = true;
    }
}

void GameOverState::keyReleased (const OIS::KeyEvent &e) {
}

void GameOverState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void GameOverState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonDown(convertMouseButton(id));
}

void GameOverState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonUp(convertMouseButton(id));
}

bool GameOverState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool GameOverState::frameEnded (const FrameEvent& evt) {
    if (_goMenu) {
        changeState(MenuState::getSingletonPtr());
    }

    if (_playAgain) {
        changeState(PlayState::getSingletonPtr());
    }

    return true;
}

GameOverState& GameOverState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

GameOverState* GameOverState::getSingletonPtr () {
    return msSingleton;
}

void GameOverState::setData (unsigned int puntuation, bool top5) {
    _puntuation = puntuation;
    _top5 = top5;
}

void GameOverState::createGUI () {
    CEGUI::Window* gameOverLabel = CEGUI::WindowManager::getSingleton().
        createWindow("TaharezLook/Label", "GameOverLabel");
    stringstream s_label;
    s_label << "[font='DejaVuSans-40']It's over!\n" <<
        "[font='DejaVuSans-25']You have reached " << _puntuation << " points.";
    if (_top5) {
        s_label << "\nCongratulations! You are in the top 5!";
    }
    gameOverLabel->setText(s_label.str());
    gameOverLabel->setSize(CEGUI::USize(CEGUI::UDim(0.7,0),CEGUI::UDim(0.7,0)));
    gameOverLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.7/2,0),CEGUI::UDim(0.4-0.7/2,0)));
    _sheet->addChild(gameOverLabel);

    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "GoMenuButton");
    backButton->setText("Go menu!");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.6-0.15/2,0),CEGUI::UDim(0.55,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::GoMenu, this));
    _sheet->addChild(backButton);

    CEGUI::Window* playAgainButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "PlayAgainButton");
    playAgainButton->setText("Play Again!");
    playAgainButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    playAgainButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4-0.15/2,0),CEGUI::UDim(0.55,0)));
    playAgainButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::PlayAgain, this));
    _sheet->addChild(playAgainButton);
}

void GameOverState::deleteGUI () {
    _sheet->destroyChild("GameOverLabel");
    _sheet->destroyChild("GoMenuButton");
    _sheet->destroyChild("PlayAgainButton");
}

CEGUI::MouseButton GameOverState::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}

bool GameOverState::PlayAgain (const CEGUI::EventArgs &e) {
    _playAgain = true;
    return true;
}

bool GameOverState::GoMenu (const CEGUI::EventArgs &e) {
    _goMenu = true;
    return true;
}
