#include <math.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsCompoundShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"
#include "OgreBulletCollisionsRay.h"

#include "Game.h"

#include "GameManager.h"

Game::Game () : _state(WAITING_NEXT_ROUND) {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    SceneNode* gameNode = _sceneMgr->createSceneNode("GameNode");
    _sceneMgr->getRootSceneNode()->addChild(gameNode);

    _worldBounds = AxisAlignedBox(Vector3 (-10000, -10000, -10000),
        Vector3 (10000,  10000,  10000));
    _gravity = Vector3(0, -9.8, 0);

    _world = new DynamicsWorld(_sceneMgr, _worldBounds, _gravity);

    #ifdef _DEBUG
        _debugDrawer = new OgreBulletCollisions::DebugDrawer();
        _debugDrawer->setDrawWireframe(true);

        SceneNode* node = _sceneMgr->getRootSceneNode()->
            createChildSceneNode("debugNode", Vector3::ZERO);
        node->attachObject(static_cast<SimpleRenderable *>(_debugDrawer));

        _world->setDebugDrawer(_debugDrawer);
        _world->setShowDebugShapes(true);
    #endif

    CreateInitialWorld();
    CreateNodePlateLauncher();
    //CreateShotgun();

    _idCurrent = 0;
    _idCartridge = 0;

    _currentAttempts = 0;

    _puntuation = 0;
    _nRound = 0;

    _timeLeftNextPlate = timeBetweenPlates;
    _timeLeftNextRound = timeBetweenRounds;
    _timeLastShoot = timeBetweenShoots;

    _endGame = false;

    CalculateNumberPlatesRound();

    _state = WAITING_NEXT_ROUND;

    _effectLaunchPlate = GameManager::getSingletonPtr()->getSoundFXManagerPtr()->load("Launch.wav");
    _effectShoot = GameManager::getSingletonPtr()->getSoundFXManagerPtr()->load("Shoot.wav");
    //_effectDestroyPlate = GameManager::getSingletonPtr()->getSoundFXManagerPtr()->load("Hit.wav");
}

Game::~Game () {
    _sceneMgr->clearScene();

    auto itPlates = _plates.begin();
    while (itPlates != _plates.end()) {
        delete *itPlates;
        ++itPlates;
    }

    auto itLaunchers = _launchers.begin();
    while (itLaunchers != _launchers.end()) {
        delete *itLaunchers;
        ++itLaunchers;
    }

    _sceneMgr->destroyCamera("camera_PlateLauncher_0");
    _sceneMgr->destroyCamera("camera_PlateLauncher_1");

    delete _bodyGround;
    delete _shapeGround;

    #ifdef _DEBUG
        delete _world->getDebugDrawer();
        _world->setDebugDrawer(nullptr);
    #endif
    delete _world;

}

void Game::LaunchPlate () {
    _effectLaunchPlate->play();
    Entity* entPlate = _sceneMgr->createEntity("plate_" +
        StringConverter::toString(_idCurrent), "plate.mesh");
    SceneNode* nodePlate = _sceneMgr->createSceneNode("plate_" +
        StringConverter::toString(_idCurrent));

    nodePlate->attachObject(entPlate);
    _sceneMgr->getSceneNode("GameNode")->addChild(nodePlate);

    AxisAlignedBox boundingB = entPlate->getBoundingBox();
    boundingB.scale(Vector3(1, 2, 1));
    CollisionShape* bodyShape = new BoxCollisionShape(boundingB.getSize()/2.0f);

    /*StaticMeshToShapeConverter* trimeshConverter = new
        StaticMeshToShapeConverter(entPlate);
    CollisionShape* bodyShape = trimeshConverter->createConvex();*/

    RigidBody* rigidBody = new RigidBody("rigidBodyPlate_" +
        StringConverter::toString(_idCurrent), _world);

    PlateLauncher* launcher = _launchers.at(rand() % _launchers.size());
    Vector3 position = launcher->getNode()->getPosition() + Vector3(0, 2, 0);
    /* Save original orientation */
    Quaternion orientation = launcher->getNode()->getOrientation();

    /* Rotate Launcher */
    launcher->rotate();

    /* Launch plate */
    rigidBody->setShape(nodePlate, bodyShape, 0.6, 0.6, 0.3, position, Quaternion::IDENTITY);
    rigidBody->setLinearVelocity(launcher->getCamera()->getDerivedDirection()
        .normalisedCopy() * (rand() % 5 + 30));

    _plates.push_back(new Plate(_idCurrent, entPlate, nodePlate, bodyShape, rigidBody));

    /* Recovery original orientation */
    launcher->getNode()->setOrientation(orientation);

    _idCurrent++;
}

void Game::UpdateWorld (const Real deltaT, int maxSubSteps) {
    _world->stepSimulation(deltaT, maxSubSteps);
    _timeLastShoot -= deltaT;
    DetectCollisionGround();

    if (_state == WAITING_NEXT_ROUND) {
        _timeLeftNextRound -= deltaT;

        if (_timeLeftNextRound <= 0) {
            _state = PLAYING;
            _timeLeftNextRound = timeBetweenRounds;
            _timeLeftNextPlate = (timeBetweenPlates - (timeLessPerRound * _nRound));
            if (_timeLeftNextPlate < 0.25) {
                _timeLeftNextPlate = 0.25;
            }
        }
    } else if (_state == PLAYING) {
        if (_nPlatesToLaunch > 0) {
            _timeLeftNextPlate -= deltaT;

            if (_timeLeftNextPlate <= 0) {
                LaunchPlate();
                _nPlatesToLaunch--;
                _timeLeftNextPlate = (timeBetweenPlates - (timeLessPerRound * _nRound));
                if (_timeLeftNextPlate < 0.25) {
                    _timeLeftNextPlate = 0.25;
                }
            }
        }

        if (_nPlates == 0) {
            _state = WAITING_NEXT_ROUND;
            _nRound++;
            CalculateNumberPlatesRound();
            _timeLeftNextRound = timeBetweenRounds;
        }
    }
}

void Game::Shoot () {
    if (_timeLastShoot < 0) {
        _effectShoot->play();
        _timeLastShoot = timeBetweenShoots;

        Vector3 p;
        Ray r;

        RigidBody* body = HitBody(p, r, 0.5, 0.5);

        if (body) {
            if (!body->isStaticObject()) {
                body->enableActiveState();
                string nameBody = body->getName();
                if (nameBody.find("rigidBodyPlate_") != std::string::npos) {
                    //_effectDestroyPlate->play();
                    std::vector<string> nameSplit = split(nameBody, '_');
                    unsigned int id = atoi(nameSplit[1].c_str());
                    DestroyPlate(id);
                    UpdateScore();
                }
            }
        }

        _idCartridge++;
    }
}

void Game::CreateInitialWorld () {
    /* Ground creation */
    SceneNode* groundNode = _sceneMgr->createSceneNode("groundNode");
    Entity* groundEnt = _sceneMgr->createEntity("groundEnt", "Ground.mesh");

    groundNode->attachObject(groundEnt);
    _sceneMgr->getSceneNode("GameNode")->addChild(groundNode);
    groundNode->setPosition(0, -0.1, 0);

    /* Creation for ground collision */
    StaticMeshToShapeConverter* trimeshConverter = new
        StaticMeshToShapeConverter(groundEnt);
    _shapeGround = trimeshConverter->createTrimesh();
    delete trimeshConverter;
    _bodyGround = new RigidBody("groundRigidBody", _world);

    //_shapeGround = new StaticPlaneCollisionShape(Ogre::Vector3(0, 1, 0), 0);
    //_bodyGround = new RigidBody("groundRigidBody", _world);

    /* Creation of static form:: form, restitution, friction */
    _bodyGround->setStaticShape(_shapeGround, 0.1, 0.8);
}

/* Creation of point where plates will be launched */
void Game::CreateNodePlateLauncher () {
    _launchers.reserve(2);
    _launchers.push_back(new PlateLauncher("PlateLauncher_0", Vector3(-7.5, 0.5, 0)));

    _launchers.push_back(new PlateLauncher("PlateLauncher_1", Vector3(7.5, 0.5, 0)));
    _sceneMgr->getSceneNode("PlateLauncher_1")->yaw(Degree(180));
}

/* Creation of the shotgun entity */
void Game::CreateShotgun () {
    SceneNode* nodeShotgun = _sceneMgr->createSceneNode("Shotgun");
    Entity* entShotgun = _sceneMgr->createEntity("ShotgunEnt", "Shotgun.mesh");

    nodeShotgun->attachObject(entShotgun);
    _sceneMgr->getRootSceneNode()->addChild(nodeShotgun);

    nodeShotgun->setPosition(_camera->getPosition());
    nodeShotgun->scale(0.2, 0.2, 0.2);
    nodeShotgun->translate(0, -0.1, 0);
}

void Game::CalculateNumberPlatesRound () {
    _nPlates = round(pow((_nRound + 1)/2.5, 1.5));//(int) pow((1 + _nRound/3.5), 0.95);
    _nPlatesToLaunch = _nPlates;
}

RigidBody* Game::HitBody (Vector3 &p, Ray &r, float x, float y) {
    r = _camera->getCameraToViewportRay (x, y);
    CollisionClosestRayResultCallback cQuery =
        CollisionClosestRayResultCallback (r, _world, 10000);
    _world->launchRay(cQuery);

    if (cQuery.doesCollide()) {
        RigidBody* body = (RigidBody *) (cQuery.getCollidedObject());
        p = cQuery.getCollisionPoint();
        return body;
    }

    return nullptr;
}

void Game::DetectCollisionGround () {
    btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i = 0; i < numManifolds; i++) {
        btPersistentManifold* contactManifold =
            bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        btCollisionObject* objA = (btCollisionObject*) (contactManifold->getBody0());
        btCollisionObject* objB = (btCollisionObject*) (contactManifold->getBody1());
        btCollisionObject* objGround = _bodyGround->getBulletObject();

        Object* obGround = _world->findObject(objGround);
        Object *obOB_A = _world->findObject(objA);
        Object *obOB_B = _world->findObject(objB);

        if ((obOB_A == obGround) || (obOB_B == obGround)) {
            SceneNode* node = nullptr;

            if ((obOB_A != obGround) && (obOB_A)) {
                node = obOB_A->getRootNode();
                //delete obOB_A;
            } else if ((obOB_B != obGround) && (obOB_B)) {
                node = obOB_B->getRootNode();
                //delete obOB_B;
            }

            if (node != nullptr) {
                //_sceneMgr->getSceneNode("GameNode")->
                //    removeAndDestroyChild(node->getName());

                PlateFailed();
            }
        }
    }
}

void Game::DestroyPlate (unsigned int id) {
    for (unsigned int i = 0; i < _plates.size(); i++) {
        Plate* plate = _plates.at(i);
        if (plate->getID() == id) {
            _plates.erase(_plates.begin() + i);
            delete plate;
            _nPlates--;
            break;
        }
    }
}

void Game::UpdateScore () {
    _puntuation += (int) (10 + ((_nRound / 3) * 2) );
}

void Game::PlateFailed () {
    //std::cout << "ASDF" << std::endl;
    _currentAttempts++;
    if (_currentAttempts >= maxAttempts) {
        _endGame = true;
    }

    for (unsigned int i = 0; i < _plates.size(); i++) {
        Plate* plate = _plates.at(i);
        delete plate;
    }
    _plates.clear();


    _puntuation -= _nRound * 0.75;
    if (_puntuation < 0) {
        _puntuation = 0;
    }

    if (_nRound > 1) {
        _nRound--;
    }

    CalculateNumberPlatesRound();

    _state = WAITING_NEXT_ROUND;
}

std::vector<string> Game::split(string str, char delimiter) {
    std::vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
}
