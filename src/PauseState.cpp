#include "PauseState.h"

#include "MenuState.h"

template<> PauseState* Singleton<PauseState>::msSingleton = 0;

void PauseState::enter () {
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);

    _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

    createGUI();

    _backToGame = false;
    _exitGame = false;
}

void PauseState::exit () {
    deleteGUI();
}

void PauseState::pause() {
}

void PauseState::resume() {
}

bool PauseState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool PauseState::frameEnded (const FrameEvent& evt) {
    if (_backToGame) {
        popState();
    }

    if (_exitGame) {
        changeState(MenuState::getSingletonPtr());
    }

    return true;
}

void PauseState::keyPressed (const OIS::KeyEvent &e) {
    /* P --> PauseState.
       ESCAPE --> ExitGame*/
    if (e.key == OIS::KC_P) {
        _backToGame = true;
    } else if (e.key == OIS::KC_ESCAPE) {
        _exitGame = true;
    }
}

void PauseState::keyReleased (const OIS::KeyEvent &e) {
}

void PauseState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void PauseState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void PauseState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

PauseState* PauseState::getSingletonPtr () {
    return msSingleton;
}

PauseState& PauseState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void PauseState::createGUI () {
    /* Puse Text */
    CEGUI::Window* pauseLabel = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "PauseLabel");
    pauseLabel->setText("[colour='FFFFFFFF'][font='DejaVuSans-40']Pause");
    pauseLabel->setSize(CEGUI::USize(CEGUI::UDim(0.4,0),CEGUI::UDim(0.1,0)));
    pauseLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.4/2,0),CEGUI::UDim(0.25,0)));
    _sheet->addChild(pauseLabel);

    /* Back to game button */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");
    backButton->setText("Back");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.4-0.15/2,0),CEGUI::UDim(0.5,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::back, this));
    _sheet->addChild(backButton);

    CEGUI::Window* goMenuButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "goMenuButton");
    goMenuButton->setText("Go Menu");
    goMenuButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    goMenuButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.6-0.15/2,0),CEGUI::UDim(0.5,0)));
    goMenuButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::goMenu, this));
    _sheet->addChild(goMenuButton);
}

void PauseState::deleteGUI () {
    _sheet->destroyChild("PauseLabel");
    _sheet->destroyChild("BackButton");
    _sheet->destroyChild("goMenuButton");
}

bool PauseState::back (const CEGUI::EventArgs &e) {
    _backToGame = true;
    return true;
}

bool PauseState::goMenu (const CEGUI::EventArgs &e) {
    _exitGame = true;
    return true;
}

CEGUI::MouseButton PauseState::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}
