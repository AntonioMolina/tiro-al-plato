#include "CreditsState.h"

template<> CreditsState* Singleton<CreditsState>::msSingleton = 0;

CreditsState::CreditsState () : _sheet(nullptr), _root(nullptr),
        _sceneMgr(nullptr), _camera(nullptr), _back(false) {
}

void CreditsState::enter () {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _back = false;
    createGUI();
}

void CreditsState::exit () {
    _root->getAutoCreatedWindow()->removeAllViewports();
    deleteGUI();
}

void CreditsState::pause () {}

void CreditsState::resume () {}

void CreditsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
        case OIS::KC_C:
            _back = true;
            break;
        default:
            break;
    }
}

void CreditsState::keyReleased (const OIS::KeyEvent &e) {}

void CreditsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void CreditsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonDown(convertMouseButton(id));
}

void CreditsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonUp(convertMouseButton(id));
}

bool CreditsState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool CreditsState::frameEnded (const FrameEvent& evt) {
    if (_back) {
        popState();
    }

    return true;
}

CreditsState& CreditsState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

CreditsState* CreditsState::getSingletonPtr () {
    return msSingleton;
}

void CreditsState::createGUI () {
    _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Credits");

    /* Back Menu button */
    CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");
    backButton->setText("Back");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&CreditsState::back, this));

    CEGUI::Window* credits = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "Credits");
    credits->setText("[font='DejaVuSans-25'][colour='FFD7D7D7']Maldonado Burgos, Julia\nMolina Maroto, Antonio");
    credits->setSize(CEGUI::USize(CEGUI::UDim(0.35,0),CEGUI::UDim(0.15,0)));
    credits->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.35/2,0),CEGUI::UDim(0.5-0.15/2,0)));

    _sheet->addChild(backButton);
    _sheet->addChild(credits);

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);
}

void CreditsState::deleteGUI () {
    _sheet->destroyChild("BackButton");

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->destroyChild(_sheet);
}

bool CreditsState::back (const CEGUI::EventArgs &e) {
    _back = true;
    return true;
}

CEGUI::MouseButton CreditsState::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}
