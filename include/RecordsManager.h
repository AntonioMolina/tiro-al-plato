#ifndef RecordsManager_H
#define RecordsManager_H

#include <fstream>
#include <iostream>
#include <sstream>

#include <Ogre.h>

using namespace Ogre;
using namespace std;

class RecordsManager : public Singleton<RecordsManager> {
    public:
        RecordsManager ();

        bool saveRecords (int puntuation);
        void createRecordsFile ();
        string createStringRecords ();

        static RecordsManager& getSingleton ();
        static RecordsManager* getSingletonPtr ();

        std::vector<std::string> split (std::string str, char delimiter);
};

#endif /* RecordsManager_H */
