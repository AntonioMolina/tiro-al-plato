#ifndef PauseState_H
#define PauseState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

using namespace Ogre;

class PauseState : public Singleton<PauseState>, public GameState {
    public:
        PauseState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

        static PauseState& getSingleton ();
        static PauseState* getSingletonPtr ();

    protected:
        CEGUI::Window* _sheet;

        Root* _root;
        SceneManager* _sceneMgr;
        Viewport* _viewport;
        Camera* _camera;

        bool _backToGame;
        bool _exitGame;

    private:
        void createGUI ();
        void deleteGUI ();

        bool back (const CEGUI::EventArgs &e);
        bool goMenu (const CEGUI::EventArgs &e);

        CEGUI::MouseButton convertMouseButton (OIS::MouseButtonID id);
};

#endif /* PauseState_H */
