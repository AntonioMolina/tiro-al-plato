#ifndef Constants_H
#define Constants_H

#include <Ogre.h>

using namespace Ogre;

enum Status {
    WAITING_NEXT_ROUND,
    PLAYING, 
};

const unsigned int maxAttempts = 5;

const Real timeLessPerRound = 0.02;
const Real timeBetweenPlates = 2;
const Real timeBetweenRounds = 4;
const Real timeBetweenShoots = 0.1;

#endif /* Constants_H */
