#ifndef GameOverState_H
#define GameOverState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"

using namespace Ogre;

class GameOverState : public Singleton<GameOverState>, public GameState {
    public:
        GameOverState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

        static GameOverState& getSingleton ();
        static GameOverState* getSingletonPtr ();

        void setData (unsigned int puntuation, bool top5);

    protected:
        CEGUI::Window* _sheet;

        Root* _root;
        SceneManager* _sceneMgr;
        Viewport* _viewport;
        Camera* _camera;

    private:
        unsigned int _puntuation;
        bool _top5;

        bool _playAgain;
        bool _goMenu;

        void createGUI ();
        void deleteGUI ();

        bool PlayAgain (const CEGUI::EventArgs &e);
        bool GoMenu (const CEGUI::EventArgs &e);

        CEGUI::MouseButton convertMouseButton (OIS::MouseButtonID id);
};

#endif /* GameOverState_H */
