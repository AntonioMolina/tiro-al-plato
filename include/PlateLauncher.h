#ifndef PlateLauncher_H
#define PlateLauncher_H

#include <Ogre.h>

using namespace Ogre;
using namespace std;

class PlateLauncher {
    public:
        PlateLauncher (const string name, Vector3 position);
        ~PlateLauncher ();

        SceneNode* getNode () const {
            return _nodePlateLauncher;
        }

        Entity* getEnt () const {
            return _entPlateLauncher;
        }

        Camera* getCamera () const {
            return _cameraPlateLauncher;
        }

        void rotate ();

    private:
        int _minRotY;
        int _maxRotY;

        int _minRotX;
        int _maxRotX;

        Root* _root;
        SceneManager* _sceneMgr;

        SceneNode* _nodePlateLauncher;
        Entity* _entPlateLauncher;
        Camera* _cameraPlateLauncher;
};

#endif /* PlateLauncher_H */
