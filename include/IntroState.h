#ifndef IntroState_H
#define IntroState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

using namespace Ogre;

class IntroState : public Singleton<IntroState>, public GameState
{
    public:
        IntroState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

        static IntroState& getSingleton ();
        static IntroState* getSingletonPtr ();

    protected:
        Root* _root;
        SceneManager* _sceneMgr;
        Viewport* _viewport;
        Camera* _camera;

    private:
        void setCEGUI ();
};

#endif /* IntroState_H */
