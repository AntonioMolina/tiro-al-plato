#ifndef MenuState_H
#define MenuState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

using namespace Ogre;

class MenuState : public Singleton<MenuState>, public GameState
{
    public:
        MenuState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();

    protected:
        CEGUI::Window* _sheet;

        Root* _root;
        SceneManager* _sceneMgr;
        Viewport* _viewport;
        Camera* _camera;

        bool _exit;

    private:
        void createBackground ();
        void deleteBackground ();

        void createGUI ();
        void deleteGUI ();

        bool start (const CEGUI::EventArgs &e);
        bool quit (const CEGUI::EventArgs &e);
        bool credits (const CEGUI::EventArgs &e);
        bool records (const CEGUI::EventArgs &e);
        bool controls (const CEGUI::EventArgs &e);

        CEGUI::MouseButton convertMouseButton (OIS::MouseButtonID id);
};

#endif /* MenuState_H */
