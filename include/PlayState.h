#ifndef PlayState_H
#define PlayState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

#include "Game.h"

using namespace Ogre;

class PlayState : public Singleton<PlayState>, public GameState {
    public:
        PlayState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();

        Game* getGame () const {
            return _game;
        }

        void deleteGame () {
            if (_game) {
                delete _game;
            }
        }

    protected:
        CEGUI::Window* _sheet;

        Real _deltaT;
        Root* _root;
        SceneManager* _sceneMgr;
        Viewport* _viewport;
        Camera* _camera;

        bool _pause;

    private:
        OverlayManager* _overlayManager;

        Game* _game;

        void repositionCamera ();

        void createGUI ();
        void deleteGUI ();
        void updateGUI ();

        void createOverlay ();
        void destroyOverlay ();

        CEGUI::MouseButton convertMouseButton (OIS::MouseButtonID id);
};

#endif /* PlayState_H */
