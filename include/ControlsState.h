#ifndef ControlsState_H
#define ControlsState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

using namespace Ogre;

class ControlsState : public Singleton<ControlsState>, public GameState {
    public:
        ControlsState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

        static ControlsState& getSingleton ();
        static ControlsState* getSingletonPtr ();

    protected:
        CEGUI::Window* _sheet;

        Root* _root;
        SceneManager* _sceneMgr;
        Viewport* _viewport;
        Camera* _camera;

        bool _back;

    private:
        void createGUI ();
        void deleteGUI ();

        bool back (const CEGUI::EventArgs &e);

        CEGUI::MouseButton convertMouseButton (OIS::MouseButtonID id);
};

#endif /* ControlsState_H */
