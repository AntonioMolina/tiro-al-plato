#ifndef Plate_H
#define Plate_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

class Plate {
    public:
        Plate (unsigned int id, Entity* ent, SceneNode* node, CollisionShape* shape,
            RigidBody* body);
        ~Plate ();

        unsigned int getID () const {
            return _id;
        }

        Entity* getEntity () const {
            return _entPlate;
        }

        SceneNode* getNode () const {
            return _nodePlate;
        }

        CollisionShape* getShape () const {
            return _shapePlate;
        }

        RigidBody* getBody () const {
            return _bodyPlate;
        }

    private:
        unsigned int _id;

        Entity* _entPlate;
        SceneNode* _nodePlate;

        CollisionShape* _shapePlate;
        RigidBody* _bodyPlate;
};

#endif /* Plate_H */
