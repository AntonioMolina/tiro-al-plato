#ifndef Game_H
#define Game_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>
#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "Constants.h"

#include "Cartridge.h"
#include "Plate.h"
#include "PlateLauncher.h"
#include "SoundFX.h"

using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;
using namespace std;

class Game {
    public:
        Game ();
        ~Game ();

        int getPuntuation () const {
            return _puntuation;
        }

        int getRound () const {
            return _nRound;
        }

        int getAttempts () const {
            return _currentAttempts;
        }

        Real getTimeLeftNextRound () const {
            return _timeLeftNextRound;
        }

        Real getTimeLeftNextPlate () const {
            return _timeLeftNextPlate;
        }

        bool getEndGame () const {
            return _endGame;
        }

        Status getState () const {
            return _state;
        }

        std::deque<Plate*> getPlates () const {
            return _plates;
        }

        void setState (Status state) {
            _state = state;
        }

        void LaunchPlate ();
        void UpdateWorld (const Real deltaT, int maxSubSteps = 1);
        void Shoot ();

    private:
        unsigned int _idCurrent;
        unsigned int _idCartridge;

        unsigned int _currentAttempts;

        unsigned int _nRound;
        unsigned int _nPlates;
        unsigned int _nPlatesToLaunch;

        unsigned int _puntuation;

        Real _timeLeftNextRound;
        Real _timeLeftNextPlate;
        Real _timeLastShoot;

        bool _endGame;

        Status _state;

        Root* _root;
        SceneManager* _sceneMgr;
        Camera* _camera;

        AxisAlignedBox _worldBounds;
        Vector3 _gravity;

        #ifdef _DEBUG
            DebugDrawer * _debugDrawer;
        #endif
        DynamicsWorld* _world;

        RigidBody* _bodyGround;
        CollisionShape* _shapeGround;

        std::vector<PlateLauncher*> _launchers;
        std::deque<Plate*> _plates;
        //std::deque<Cartridge*> _cartridges;

        void CreateInitialWorld ();
        void CreateNodePlateLauncher ();
        void CreateShotgun ();

        void CalculateNumberPlatesRound ();
        RigidBody* HitBody (Vector3 &p, Ray &r, float x, float y);

        void DetectCollisionGround ();

        void DestroyPlate (unsigned int id);
        void UpdateScore ();
        void PlateFailed ();

        std::vector<string> split(string str, char delimiter);

        SoundFXPtr _effectLaunchPlate;
        SoundFXPtr _effectDestroyPlate;
        SoundFXPtr _effectShoot;
};

#endif /* Game_H */
