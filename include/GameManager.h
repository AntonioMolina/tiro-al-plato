#ifndef GameManager_H
#define GameManager_H

#include <stack>

#include <Ogre.h>
#include <OgreSingleton.h>
#include <OIS/OIS.h>

#include <OgreOverlaySystem.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>

#include "InputManager.h"
#include "Track.h"
#include "TrackManager.h"
#include "SoundFXManager.h"

using namespace Ogre;

class GameState;

class GameManager : public FrameListener, public Singleton<GameManager>,
        public OIS::KeyListener, public OIS::MouseListener {
    public:
        GameManager ();
        ~GameManager (); /* Limpieza de todos los estados. */

        /* Para el estado inicial. */
        void start (GameState* state);

        /* Funcionalidad para transiciones de estados. */
        void changeState (GameState* state);
        void pushState (GameState* state);
        void popState ();

        /* Heredados de Singleton. */
        static GameManager& getSingleton ();
        static GameManager* getSingletonPtr ();

        TrackManager* getTrackManagerPtr() { return _pTrackManager; }
        SoundFXManager* getSoundFXManagerPtr() { return _pSoundFXManager; }

        TrackPtr getTrackMenu () { return _pTrackMenu; }
        TrackPtr getTrackGame () { return _pTrackJuego; }

    protected:
        Root* _root;
        SceneManager* _sceneMgr;
        RenderWindow* _renderWindow;

        /* Funciones de configuración. */
        void loadResources ();
        bool configure ();

        /* Heredados de FrameListener. */
        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

    private:
        /* Funciones para delegar eventos de teclado
           y ratón en el estado actual. */
        bool keyPressed (const OIS::KeyEvent &e);
        bool keyReleased (const OIS::KeyEvent &e);

        bool mouseMoved (const OIS::MouseEvent &e);
        bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool initSDL ();

        /* Gestor de eventos de entrada. */
        InputManager *_inputMgr;
        /* Estados del juego. */
        std::stack<GameState*> _states;

        // Manejadores del sonido.
        TrackManager* _pTrackManager;
        SoundFXManager* _pSoundFXManager;

        // Musica para menús y juego
        TrackPtr _pTrackMenu;
        TrackPtr _pTrackJuego;
};

#endif /* GameManager_H */
